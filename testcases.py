#!/usr/bin/python
# -*- coding: utf-8 -*-

from BeautifulSoup import BeautifulSoup

def get_tests(xml):
  soup = BeautifulSoup(open(xml))
  tests = {}
  for entry in soup.findAll('entry'):
    key = entry.find('title').string
    tests[key] =entry.find('content').contents[0]
  return tests

if __name__ == '__main__':
  print get_tests('papers.xml')

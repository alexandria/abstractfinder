#!/usr/bin/python
# -*- coding: utf-8 -*-


# http
import requests
from urllib import quote_plus
# html
from BeautifulSoup import BeautifulSoup

# system
import subprocess, sys
from os import remove

# regex
import re, string

# unit tests
import testcases

def get_scholar_results(query):
  r = requests.get('http://scholar.google.fr/scholar?q=' + quote_plus(query))
  return BeautifulSoup(r.content)

def flatten_text(tags):
  str = ""
  for t in tags:
    if t.string is None:
      str += flatten(t.contents)
    else:
      str += t.string
  return str
      

def get_pdf_url(page, wanted_title):
  for elem in page.findAll('div', {"class": "gs_r"}):
    try:
      found_title = flatten_text(elem.find('div', {"class": "gs_rt"}).find('a').contents)
      if wanted_title.lower() == found_title.lower():
        return elem.find('span', {"class": "gs_ggs gs_fl"}).find('a')['href']
    except: pass

def postprocess(abstract):
  lines_kept = []
  for line in abstract.split('.'):
    found_wrong = False
    for wrong in ['Categories and Subject', 'General Terms', 'Key Words']:
      if wrong in line:
        found_wrong = True
    if found_wrong: break
    else: lines_kept.append(line.strip())
  final_abstract = '. '.join(lines_kept).strip()
  if final_abstract[-1] != '.': final_abstract += '.'
  return final_abstract

def get_abstract(pdf_url):
  r = requests.get(pdf_url)
  f = open('/tmp/file.pdf', 'w')
  f.write(r.content)
  f.close()

  abstract = ''

  retcode = subprocess.call(["pdftotext", "/tmp/file.pdf"])
  #remove("/tmp/file.pdf")
  if retcode is 0:
    for line in open('/tmp/file.txt'):
      if len(line) > 100:
        abstract = postprocess(line)
        break
  else: raise "pdftotext fail"
  remove("/tmp/file.txt")
  return abstract

def same_abstract(a1, a2):
  a1_cleaned = re.sub(r"[%s]"%string.punctuation, '', a1)
  a2_cleaned = re.sub(r"[%s]"%string.punctuation, '', a2)

  return a1_cleaned == a2_cleaned


if __name__ == '__main__':
  for wanted_title, wanted_abstract in testcases.get_tests('papers.xml').iteritems():
    try:
      print wanted_title,
      pdf_url = get_pdf_url(get_scholar_results(wanted_title), wanted_title)
  
      if pdf_url is None: raise Exception('pdf', "no pdf found from Google Scholar, sorry")

      found_abstract = get_abstract(pdf_url)
      if not same_abstract(found_abstract, wanted_abstract): raise Exception('abstract', found_abstract, wanted_abstract)
      print "yes"
    except: print "no"


